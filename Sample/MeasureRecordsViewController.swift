//
//  MeasureRecordsViewController.swift
//  Sample
//
//  Created by Isham on 26/01/2019.
//  Copyright © 2019 Isham. All rights reserved.
//

import UIKit
import CoreData

class MeasureRecordsViewController: UIViewController {
    
    @IBOutlet weak var dateTimeHeaderLabel: UIButton!
    
    @IBOutlet weak var systolicSlider: UISlider!
    
    @IBOutlet weak var heartRateSlider: UISlider!
    @IBOutlet weak var diastolicSlider: UISlider!
    
    @IBOutlet weak var systolicLabel: UIButton!
    
    @IBOutlet weak var heartRateLabel: UIButton!
    @IBOutlet weak var diastolicLabel: UIButton!
    
    
    @IBOutlet weak var measurementLabel: UILabel!
    @IBOutlet weak var entryfieldView: UIView!
    @IBOutlet weak var entryFieldTextField: UITextField!
    
    let list = ["Blood pressure","Blood glucose","Pulse","Temperature","Weight"]
    
    var viewType : Int = 0
    private var measurement: MeasurementData! = MeasurementData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = list[viewType]
        
        dateTimeHeaderLabel.layer.borderWidth = 1.0
        dateTimeHeaderLabel.layer.cornerRadius = 8.0
        dateTimeHeaderLabel.layer.borderColor = UIColor.black.cgColor
        dateTimeHeaderLabel.clipsToBounds = true
        
        systolicLabel.layer.borderWidth = 1.0
        systolicLabel.layer.cornerRadius = 8.0
        systolicLabel.layer.borderColor = UIColor.black.cgColor
        systolicLabel.clipsToBounds = true
        systolicLabel.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
        diastolicLabel.layer.borderWidth = 1.0
        diastolicLabel.layer.cornerRadius = 8.0
        diastolicLabel.layer.borderColor = UIColor.black.cgColor
        diastolicLabel.clipsToBounds = true
        diastolicLabel.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
        heartRateLabel.layer.borderWidth = 1.0
        heartRateLabel.layer.cornerRadius = 8.0
        heartRateLabel.layer.borderColor = UIColor.black.cgColor
        heartRateLabel.clipsToBounds = true
        heartRateLabel.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
        systolicLabel.layer.borderWidth = 1.0
        systolicLabel.layer.cornerRadius = 8.0
        systolicLabel.layer.borderColor = UIColor.black.cgColor
        systolicLabel.clipsToBounds = true
        systolicLabel.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, dd LLLL HH:mm"
        dateTimeHeaderLabel.setTitle(dateFormatter.string(from: Date()), for: .normal)
        dateTimeHeaderLabel.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
        systolicSlider.minimumValue = 100.0
        systolicSlider.maximumValue = 160.0
        
        diastolicSlider.minimumValue = 100.0
        diastolicSlider.maximumValue = 160.0
        
        heartRateSlider.minimumValue = 50
        heartRateSlider.maximumValue = 110
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "SAVE", style: .done, target: self, action: #selector(onSaveClicked))
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        if(viewType != 0){
            entryfieldView.isHidden = false
            entryFieldTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            
            entryFieldTextField.layer.borderColor = UIColor.black.cgColor
            entryFieldTextField.layer.borderWidth = 1.0
            entryFieldTextField.layer.cornerRadius = 8.0
            entryFieldTextField.borderStyle = UITextField.BorderStyle.roundedRect
            entryFieldTextField.clipsToBounds = true
        }
        self.getMeasurementDetails()
    }
    
    @IBAction func onSystolicValueChanged(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        systolicLabel.setTitle(String(currentValue), for: .normal)
    }
    
    @IBAction func onHeartRateValueChanged(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        heartRateLabel.setTitle(String(currentValue), for: .normal)
    }
    
    @IBAction func onDiastolicValueChanged(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        diastolicLabel.setTitle(String(currentValue), for: .normal)
    }
    
    @objc func onSaveClicked() {
        measurement.measurementDateTime = getCurrentDateTimeString() //        2019-02-09T07:20:34.987Z"
        
        if(viewType != 0){
            measurement.value = entryFieldTextField.text!
        }else{
            let systolicVal = Int(systolicSlider.value)
            let diaSystolicVal = Int(diastolicSlider.value)
            let heartRateValue = Int(heartRateSlider.value)
            measurement.value =  String(systolicVal) + "/" + String(diaSystolicVal) + "/" + String(heartRateValue)
        }
                CoreDataUtils().saveMeasurement(tablename: measurement.measurementName!, measurement: measurement)
        NetworkManager().uploadData(measurementData: measurement ){ (response) -> () in
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func getMeasurementDetails() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MeasurementType")
        var tableName = ""
        if(self.list[viewType] == "Blood pressure"){
            tableName = "BloodPressure"
        }else if (self.list[viewType] == "Blood glucose"){
            tableName = "BloodGlucose"
        }else{
            tableName = self.list[viewType]
        }
        
        measurement.measurementName = tableName;
        
        request.predicate = NSPredicate(format: "name = %@",  tableName)
        request.returnsObjectsAsFaults = false
        
        measurement.isManual = true
//        measurement.measurementDateTime = "2019-02-07T17:20:34.987Z"
        
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                //                print(data.value(forKey: "username") as! String)
                measurement.measurementTypeId = data.value(forKey: "id") as? String
                measurement.measurementUnit = data.value(forKey: "unit") as? String
                
            }
        } catch {
            print("Failed")
        }
        
        let patientDetailsRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(patientDetailsRequest)
            for data in result as! [NSManagedObject] {
                measurement.patientId = data.value(forKey: "id") as? String
            }
        } catch {
            print("Failed")
        }
        //hardcoded
        measurementLabel.text = "mgdl"
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
    }
    
    
    func getCurrentDateTimeString() -> String{
        let currentDateTime = Date()
        let dateTimeFormatter = DateFormatter()
        dateTimeFormatter.dateFormat="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        //        dateTimeFormatter.timeZone = TimeZone(identifier: "UTC")
        let str:String  = dateTimeFormatter.string(from: currentDateTime)
        print("DATE-TIME \(str)")
        return str
    }
}

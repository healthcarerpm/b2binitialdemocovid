//
//  RoundRectView.swift
//  MyFetal Life
//
//  Created by Om Bhagwan on 21/05/18.
//  Copyright © 2018 Om Bhagwan. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class RoundRectView : UIView {
    
    @IBInspectable var viewRoundCorner : CGFloat = 0.0
    @IBInspectable var viewBorderWidth :CGFloat = 0.0
    @IBInspectable var viewBorderColor : UIColor = UIColor.clear
    
    override func draw(_ rect: CGRect) {
        self.layer.cornerRadius = viewRoundCorner
        self.layer.borderWidth = viewBorderWidth
        self.layer.borderColor = viewBorderColor.cgColor
    }
}

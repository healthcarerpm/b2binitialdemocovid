//
//  PDFReportViewController.swift
//  Sample
//
//  Created by Thejus Jose on 05/05/20.
//  Copyright © 2020 Isham. All rights reserved.
//

import Foundation
import SWRevealViewController

class PDFReportViewController : UIViewController {
    
    @IBOutlet var generateButton: BasicButton!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var commentsTextView: UITextView!
    @IBOutlet weak var sideMenu: UIBarButtonItem!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet weak var endDate: UITextField!
    @IBOutlet weak var StartDate: UITextField!
    var keyboardHeight : CGFloat?
    
    var pdfReport:Data? = nil
    
    lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker();
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(datePickerChanged(_:)), for: .valueChanged)
        return picker
    }()
    
    @objc func datePickerChanged(_ sender: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        StartDate.text = dateFormatter.string(from: sender.date)
    }
    
    
    lazy var datePicker2: UIDatePicker = {
        let picker = UIDatePicker();
        picker.datePickerMode = .date
        picker.maximumDate = Date()
        picker.addTarget(self, action: #selector(datePickerChangedend(_:)), for: .valueChanged)
        return picker
    }()
    
    @objc func datePickerChangedend(_ sender: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        endDate.text = dateFormatter.string(from: sender.date)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StartDate.doneAccessory = true
        endDate.doneAccessory = true
        
        StartDate.inputView = datePicker
        self.view.addBackgroundImage()
        endDate.inputView = datePicker2
        if self.revealViewController() != nil {
            sideMenu.target = self.revealViewController()
            sideMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    func showAlert (title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil);
    }
    
    @IBAction func generateButton_Tapped(_ sender: Any) {
        
        let patientId = UserDefaults.standard.value(forKey: Constants.USER_DEF_PATIENT_ID);
        let toDate = Date();
        //7 dayes before toDate
        let fromDate = toDate.addingTimeInterval(-7*24*60*60)
        
        NetworkManager().getAssessmentRangeData(patientId: patientId as! String, fromDate: fromDate, toDate: toDate, start: 0, count: 0) { (responseData) in
            //                print("responseDate=\(responseDate)")
            let totalItemCount:Int = responseData!.get("TotalItemCount").int ?? 0
            var sections = [PDFSection]()
            for eachAssessment in responseData!.get("PageItems") ?? JSONTraverser() {
                var secText = ""
                for question in eachAssessment.get("Questions") ?? JSONTraverser() {
                    secText.append(question.get("Text").string!)
                    secText.append("\n")
                }
                var titleText = eachAssessment.get("Name").string!
                //"AnsweredDateTime":"2020-03-17T19:46:13",
                if(eachAssessment.get("AnsweredDateTimedate").value != nil){
                    let dateString = eachAssessment.get("AnsweredDateTimedate").string!
                    let dateFormatter = DateFormatter()
                    //        var date:Date? = nil
                    dateFormatter.locale = Locale.current
                    // save locale temporarily
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                    var date = dateFormatter.date(from:dateString)
                    if(date != nil){
                        titleText.append(" ")
                        titleText.append("Taken On: \(date)")
                    }
                }
                //                print("assessment:\(eachAssessment.get("Name"))")
                sections.append(PDFSection(title: titleText, body: secText))
                
            }
            
            //                if(secCount%2 == 0){
            //                    sections.append(PDFSection(title: question.get("Text").string!, body: "",type:PDFSection.PDFSectionType.text))
            //                }else{
            //                    sections.append(PDFSection(title: question.get("Text").string!, body: "",type:PDFSection.PDFSectionType.subText))
            //                }
            //                secCount += 1
            
            //                let pdfCreator = PDFGenerator()
            let patientDetails = CoreDataUtils().getUserDetails();
            
            let patientNameString = "\(patientDetails.firstName!) \(patientDetails.lastName!)"
            
            //                self.pdfReport = pdfCreator.createPatientReport(mainTitle:"Patient Report",subTitle:patientNameString ,sections: sections)
            
            let pdfData = PDFCreator().createPatientReport(mainTitle: "COVID-19", subTitle: "Containment and mitigation", sections: sections)
            self.pdfReport = pdfData
            DispatchQueue.main.async {
                print("ALAMOFIRE:It's on main queue now")
                if(self.pdfReport != nil){
                    self.performSegue(withIdentifier: "previewPDFSegue", sender: sender)
                    //                    let previewViewController = PDFPreviewViewController()
                    //                    //previewViewController.documentData  = self.pdfReport
                    //
                    //                    let pdfCreator = PDFCreator(title: "ASDASD", body: "SADSAD",
                    //                                            contact: "asdasd")
                    //                    previewViewController.documentData = pdfCreator.createFlyer()
                    //                    self.navigationController?.pushViewController(previewViewController, animated: true)
                }
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if StartDate.text!.isEmpty || endDate.text!.isEmpty{
            self.showAlert(title: "Info", message: "Please select the date range")
        }
        else if segue.identifier == "previewPDFSegue" {
            if let destinationVC = segue.destination as? PDFPreviewViewController {
                destinationVC.documentData  = self.pdfReport
            }
        }
    }
    
    func keyBoardHeight() -> CGFloat {
        return keyboardHeight ?? 150
    }
    
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        keyboardHeight = keyBoardHeight()
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardHeight ?? keyboardSize.height/1.3//keyboardSize.height/2
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
}

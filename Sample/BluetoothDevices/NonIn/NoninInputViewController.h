//
//  NoninInputViewController.h

//
//  Created by Keith Thomas on 4/7/16.
//  Copyright © 2016 OdysseyInc, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    ProcessStateNPOInstruction = 1,
    ProcessStateNPOConnecting = 2,
    ProcessStateNPOMeasuring = 3,
    ProcessStateNPOMeasurementComplete = 4,
    ProcessStateNPOErrorNoBluetooth = 5,
    ProcessStateNPOErrorFingerNotInserted = 6,
    ProcessStateNPOMeasurementCompleteAndDisconnected = 7,
    ProcessStateNPOErrorLowBattery = 8,
    ProcessStateNPOErrorDeviceNotDetectedTimeout = 9
} ProcessStateNoninPulseOxType;


@protocol NoninInputViewControllerDelegate;

@interface NoninInputViewController : UIViewController

@property (nonatomic, weak) id<NoninInputViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *pulseLabel;
@property (weak, nonatomic) IBOutlet UIView *satUnderline;
@property (weak, nonatomic) IBOutlet UILabel *satLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property(nonatomic, assign, nullable) UIViewController* callingController;

@property (weak, nonatomic) IBOutlet UILabel *satUnitLabel;
@property (weak, nonatomic) IBOutlet UILabel *satTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *pulseUnitLabel;
@property (weak, nonatomic) IBOutlet UILabel *pulseTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *pulseUnderline;
@property (weak, nonatomic) IBOutlet UIImageView *handImage;
@property (weak, nonatomic) IBOutlet UIToolbar *bottomToolbar;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (nonatomic, strong, nullable) NSNumber *isfromHealthSession;

@property (weak, nonatomic) IBOutlet UIView *errorTimeoutExplanation;

- (IBAction)donePressed:(id)sender;
- (IBAction)cancelPressed:(id)sender;

@end

@protocol NoninInputViewControllerDelegate <NSObject>

- (void) noninInputController:(NoninInputViewController*)controller didCompleteWithMeasurement:(NSDictionary*)measurements;

@end


//
//  AnDWeightScaleViewController.h
//
//  Created by Bill Johnson on 2/14/17.
//  Copyright © 2017 OdysseyInc, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WeightScaleMeasurementCaptureProtocol

-(void)weightScaleDidCaptureMeasurement:(double)value
                           dateCaptured:(NSDate* _Nonnull)date;

@end


@interface AnDWeightScaleViewController : UIViewController

@property(nonatomic, weak, nullable) id<WeightScaleMeasurementCaptureProtocol> delegate;
@property(nonatomic, assign) UIViewController* _Nullable callingController;

@end

//
//  AnDWeightScaleViewController.m
//
//  Created by Bill Johnson on 2/14/17.
//  Copyright © 2017 OdysseyInc, LLC. All rights reserved.
//

#import "AnDWeightScaleViewController.h"
#import "ADWeightScale.h"
#import "ANDBLEDefines.h"
#import "ANDDevice.h"
#import "WCTime.h"



@interface AnDWeightScaleViewController () <ANDDeviceDelegate>

@property ANDDevice *device;
//@property NSString *type;
@property Boolean haveWeightScale;
@property (weak, nonatomic) IBOutlet UILabel *instructionTextLabelOutlet;
@property (weak, nonatomic) IBOutlet UIButton *doneButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *backButtonOutlet;
@property (weak, nonatomic) IBOutlet UIView *weightPanelOutlet;
@property (weak, nonatomic) IBOutlet UILabel *weightMeasurementTextLabelOutlet;

@end

@implementation AnDWeightScaleViewController
{
    NSTimer *deviceNotFoundTimer;
    double weightValue;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.haveWeightScale = NO;
    [self startTimer];
    
    //strcpy(0, "bla"); // if you want to crash on purpose...
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"viewWillAppear");
    
    self.doneButtonOutlet.hidden = NO;
    self.backButtonOutlet.hidden = NO;;
    self.weightPanelOutlet.hidden = YES;
    
    self.device = [ANDDevice new];
    [self.device controlSetup];
    self.device.delegate = self;
    if (self.device.activePeripheral.state != CBPeripheralStateConnected) {
        if (self.device.activePeripheral) {
            self.device.peripherials = nil;
        }
        [self.device findBLEPeripherals];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButtonActionOC:(UIButton *)sender {
    [self.delegate weightScaleDidCaptureMeasurement:weightValue dateCaptured:[NSDate date]];
   
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
         if (self.callingController) {
              [self.navigationController popToViewController:self.callingController animated:YES];
          } else {
              [self.navigationController popToRootViewControllerAnimated:YES];
          }
    });
    
}

- (IBAction)backButtonAction:(id)sender {
    
}

- (void) gotDevice:(CBPeripheral *)peripheral
{
    if (peripheral.name != nil) {
        if ([peripheral.name rangeOfString:@"A&D_UC-352"].location != NSNotFound) {
            NSLog(@"ws - %@", peripheral.name);
            self.haveWeightScale = YES;
            [self.device connectPeripheral:peripheral];
        }
    }
}

- (void) deviceReady
{
    NSLog(@"deviceReady called ");
    if (self.haveWeightScale) {
        ADWeightScale *ws = [[ADWeightScale alloc] initWithDevice:self.device];
        NSLog(@"WS Device");
    //    [ws setTime];
        [ws readMeasurement];
    }
}


- (void)gotWeight:(NSDictionary *)data
{
    NSLog(@"gotWeight");
    [self.device.CM cancelPeripheralConnection:self.device.activePeripheral];
    double weight = [[data valueForKey:@"weight"] doubleValue];
    NSString* wtUnit = [data valueForKey:@"unit"];
    weightValue = weight;
    self.doneButtonOutlet.hidden = NO;
    self.backButtonOutlet.hidden = YES;
    [self clearTimer];
    
//    self.instructionTextLabelOutlet.text = @"Measurement complete! \n\nPress Done to continue.";
    self.weightMeasurementTextLabelOutlet.text = [NSString stringWithFormat:@"%.1f %@", weight, wtUnit];
    self.weightPanelOutlet.hidden = NO;
}



#pragma mark timer

- (void) startTimer;
{
    [self clearTimer];
    deviceNotFoundTimer = [NSTimer scheduledTimerWithTimeInterval:45 target:self selector:@selector(timeExpired) userInfo:nil repeats:NO];
}

- (void) timeExpired;
{
    self.instructionTextLabelOutlet.text = @"A weight measurement has not been received from the A&D scale.  Please step on the scale and follow the instructions.";
    [self clearTimer];
}

- (void) clearTimer;
{
    if (deviceNotFoundTimer != nil) {
        [deviceNotFoundTimer invalidate];
    }
    deviceNotFoundTimer = nil;
}

@end


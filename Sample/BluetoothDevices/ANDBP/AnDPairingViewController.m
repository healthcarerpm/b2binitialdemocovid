//
//  AnDPairingViewController.m
//
//  Created by Bill Johnson on 5/1/17.
//  Copyright © 2017 OdysseyInc, LLC. All rights reserved.
//

#import "AnDPairingViewController.h"
#import "ANDBLEDefines.h"
#import "ANDDevice.h"
#import "ADBloodPressure.h"
#import "ADWeightScale.h"


@interface AnDPairingViewController () <ANDDeviceDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) ANDDevice *device;
@property (nonatomic, strong) NSMutableDictionary *devices;
@property NSString *type;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *doneButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@end


@implementation AnDPairingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.doneButtonOutlet setEnabled:NO];
    
    self.pairYourAndDevice.text = NSLocalizedString(@"Pair your A&D device", "");
    self.pairingDevice.text = NSLocalizedString(@"Put the device in pairing mode and select it from the list below.", "");
    
    [self.doneButtonOutlet setTitle:NSLocalizedString(@"Done", "") forState:UIControlStateNormal];
    [self.cancelBtn setTitle: NSLocalizedString(@"Cancel", "") forState:UIControlStateNormal];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.devices = [NSMutableDictionary new];
    
    self.device = [ANDDevice new];
    [self.device controlSetup];
    self.device.delegate = self;
    
    if (self.device.activePeripheral.state != CBPeripheralStateConnected) {
        if (self.device.activePeripheral) self.device.peripherials = nil;
        [self.device findBLEPeripherals];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - button IBActions

- (IBAction)doneButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - ANDDeviceDelegate

- (void)gotDevice:(CBPeripheral *)peripheral
{
    NSLog(@"peripheral name is %@", peripheral.name);
    if ([peripheral.name rangeOfString:@"A&D"].location != NSNotFound ||
        [peripheral.name rangeOfString:@"Murata"].location != NSNotFound ||
        [peripheral.name rangeOfString:@"Life Trak Zone"].location != NSNotFound) {
        if (![self.devices objectForKey:peripheral.name]) {
            if (peripheral.name != nil) {
                [self.devices setValue:peripheral forKey:peripheral.name];
                [self.tableView reloadData];
            }
        }
    }
}

- (void) deviceReady
{
    self.device.connectionStats = @"Connected";
    if ([self.type isEqual:@"bp"])
    {
        NSLog(@"Enter bp device");
        ADBloodPressure *bp = [[ADBloodPressure alloc] initWithDevice:self.device];
        [bp readMeasurementForSetup];
    }else if ([self.type isEqual:@"ws"])
    {
        NSLog(@"Enter WS device");
        ADWeightScale *ws = [[ADWeightScale alloc]initWithDevice:self.device];
        [ws readMeasurementForSetup];
    }
    [self.device readDeviceInformation];
}



#pragma mark - Table view delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.devices count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [UITableViewCell new];
    }
    // Configure the cell...
    for (NSString* name in [self.devices allKeys]) {
        cell.textLabel.text = name;
    }
    //  cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //  cell.textLabel.text = self.device.connectionStats;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBPeripheral *device = [self.devices objectForKey:[tableView cellForRowAtIndexPath:indexPath].textLabel.text];
    
    //  ANDDevice *andDevice = [[ANDDevice alloc] init];
    //Setting the type of the device
    if (device.name != nil) {
        if ([device.name rangeOfString:@"A&D_UC-352"].location != NSNotFound)
        {
            
            NSLog(@"Device selected is weight scale");
            self.type = @"ws";
            [self.doneButtonOutlet setEnabled:YES];
            
        } else if ([device.name rangeOfString:@"651"].location != NSNotFound || [device.name rangeOfString:@"BLP"].location != NSNotFound)
        {
            NSLog(@"Device selected is bp cuff");
            self.type = @"bp";
            [self.doneButtonOutlet setEnabled:YES];
            
        } else {
            self.type = @"unknown";
        }
        
    }
    [self.device connectPeripheral:device];
}


@end

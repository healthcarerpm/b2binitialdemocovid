//
//  AnDPairingViewController.h
//
//  Created by Bill Johnson on 5/1/17.
//  Copyright © 2017 OdysseyInc, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnDPairingViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *pairYourAndDevice;
@property (weak, nonatomic) IBOutlet UILabel *pairingDevice;




@end
